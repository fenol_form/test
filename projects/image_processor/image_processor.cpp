#include "BmpController.h"
#include "Image.h"

//DEBUG
#include <iostream>
std::string input_path = "../examples/example.bmp";
std::string output_path = "../outputs/result.bmp";
//DEBUG

int main(int argc, char *argv[]) {
    Image img;
    try {
        BmpController::GetImage(img, input_path);
    } catch(std::string s) {
        std::cout << s << std::endl;
    }

    try {
        BmpController::Load(img, output_path);
    } catch(std::string s) {
        std::cout << s << std::endl;
    }
    return 0;
}