#pragma once

#include "stdint.h"
#include <algorithm>

struct Pixel {
    uint8_t red = 0;
    uint8_t green = 0;
    uint8_t blue = 0;

    Pixel();

    Pixel(uint8_t red, uint8_t green, uint8_t blue);
};
