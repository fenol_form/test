#include "Image.h"

Image::Image() {
}

Image::Image(const ColorsMap &colorsMap, size_t height, size_t width) : data_(colorsMap), height_(height),
                                                                        width_(width) {
}

Image::Image(const Image &other) {
    (*this) = other;
}

size_t Image::GetHeight() const {
    return height_;
}

size_t Image::GetWidth() const {
    return width_;
}

const Pixel& Image::GetPixel(size_t row, size_t column) const {
    return data_[row][column];
}

void Image::SetPixel(size_t row, size_t column, const Pixel& px) {
    data_[row][column] = px;
}