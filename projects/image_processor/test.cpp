#define CATCH_CONFIG_MAIN
#define CATCH_CONFIG_FAST_COMPILE
#include "catch/catch.hpp"

#include "BmpController.h"
#include "Image.h"
#include "Pixel.h"

TEST_CASE("BmpController") {
    Image img;
    try {
        BmpController::GetImage(img, "./all_gray.bmp");
        throw "impossible exception";
    } catch(const std::string& exception) {
        REQUIRE(exception == "failed open");
    }

    BmpController::GetImage(img, "../examples/all_gray.bmp");

    REQUIRE(img.GetHeight() == 2);
    REQUIRE(img.GetWidth() == 8);

    std::vector<Pixel> expected_colors(8);
    for (size_t i = 0; i < 8; ++i) {
        expected_colors[i] = img.GetPixel(0, i);
    }

    try {
        BmpController::Load(img, "../output/all_gray_result.bmp");
        throw "impossible exception";
    } catch (const std::string& exception) {
        REQUIRE(exception == "failed open");
    }

    BmpController::Load(img, "../outputs/all_gray_result.bmp");
    BmpController::GetImage(img, "../outputs/all_gray_result.bmp");

    for (size_t i = 0; i < 8; ++i) {
        REQUIRE(expected_colors[i].blue == img.GetPixel(0, i).blue);
        REQUIRE(expected_colors[i].red == img.GetPixel(0, i).red);
        REQUIRE(expected_colors[i].green == img.GetPixel(0, i).green);
    }

}