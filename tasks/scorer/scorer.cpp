#include "scorer.h"

bool CompareEvents(const Event* a, const Event* b) {
    return std::tie(a->student_name, a->task_name, a->time) < std::tie(b->student_name, b->task_name, b->time);
}

ScoreTable GetScoredStudents(const Events& events, time_t score_time) {
    std::vector<const Event*> sort_events;
    ScoreTable result;
    for (size_t i = 0; i < events.size(); ++i) {
        if (events[i].time <= score_time) {
            sort_events.push_back(&events[i]);
        }
    }
    sort(sort_events.begin(), sort_events.end(), CompareEvents);
    bool check_success = false;
    bool merge_close = true;
    for (size_t i = 0; i < sort_events.size(); ++i) {
        if (i > 0 && (sort_events[i]->student_name != sort_events[i - 1]->student_name ||
                      sort_events[i]->task_name != sort_events[i - 1]->task_name)) {
            if (check_success && merge_close) {
                result[sort_events[i - 1]->student_name].insert(sort_events[i - 1]->task_name);
            }
            check_success = false;
            merge_close = true;
        }
        switch (sort_events[i]->event_type) {
            case EventType::CheckSuccess:
                check_success = true;
                break;
            case EventType::MergeRequestClosed:
                merge_close = true;
                break;
            case EventType::MergeRequestOpen:
                merge_close = false;
                break;
            case EventType::CheckFailed:
                check_success = false;
                break;
        }
    }
    if (check_success && merge_close) {
        result[sort_events[sort_events.size() - 1]->student_name].insert(
            sort_events[sort_events.size() - 1]->task_name);
    }
    return result;
}
