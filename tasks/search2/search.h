#pragma once

#include <string_view>
#include <vector>
#include <set>
#include <algorithm>
#include <map>

struct Document {
    std::map<std::string, size_t> words_appearance;
    std::string original_string;
    size_t word_count;
};

void SplitDocks(std::vector<Document>& documents, const std::string_view& text);

void SplitWords(Document& document, const std::string_view& text);

size_t SubstrCount(const Document& document, const std::string& sub);

void ToLower(std::string& str);

class SearchEngine {
public:
    void BuildIndex(std::string_view text);
    std::vector<std::string_view> Search(std::string_view query, size_t results_count) const;

private:
    std::vector<Document> documents_;

    void CountTfIdf(std::vector<std::pair<double, size_t>>& tf_idf, const Document& words) const;
};