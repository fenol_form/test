#include "search.h"

void SearchEngine::BuildIndex(std::string_view text) {
    documents_.clear();
    SplitDocks(documents_, text);
}

std::vector<std::string_view> SearchEngine::Search(std::string_view query, size_t results_count) const {
    Document words;
    std::vector<std::string_view> result;
    SplitWords(words, query);

    std::vector<std::pair<double, size_t>> tf_idf(documents_.size(), {0, 0});
    for (size_t i = 0; i < tf_idf.size(); ++i) {
        tf_idf[i].second = i;
    }

    CountTfIdf(tf_idf, words);

    std::sort(
        tf_idf.begin(), tf_idf.end(),
        [](const std::pair<double, int>& a, const std::pair<double, int>& b) -> bool { return a.first > b.first; });

    for (size_t i = 0; i < std::min(results_count, tf_idf.size()); ++i) {
        if (tf_idf[i].first <= 0) {
            break;
        }
        result.push_back(documents_[tf_idf[i].second].original_string);
    }

    return result;
}

void SplitDocks(std::vector<Document>& documents, const std::string_view& text) {
    size_t last = 0;
    size_t cur = 0;
    for (size_t i = 0; i < text.size(); ++i) {
        if (text[i] == '\n') {
            std::string_view buf = text;
            buf.remove_prefix(last);
            buf.remove_suffix(text.size() - cur);
            documents.push_back({});
            documents[documents.size() - 1].original_string = static_cast<std::string>(buf);
            SplitWords(documents[documents.size() - 1], buf);
            last = cur + 1;
        }
        ++cur;
    }
    std::string_view buf = text;
    buf.remove_prefix(last);
    if (!buf.empty()) {
        documents[documents.size() - 1].original_string = static_cast<std::string>(buf);
        SplitWords(documents[documents.size() - 1], buf);
    }
}

void SplitWords(Document& document, const std::string_view& text) {
    size_t last = 0;
    size_t cur = 0;
    size_t word_count = 0;
    for (size_t i = 0; i < text.size(); ++i) {
        if (!isalpha(text[i])) {
            std::string_view buf = text;
            buf.remove_prefix(last);
            buf.remove_suffix(text.size() - cur);
            std::string val = static_cast<std::string>(buf);
            ToLower(val);
            ++document.words_appearance[val];
            ++word_count;
            last = cur + 1;
        }
        ++cur;
    }
    std::string_view buf = text;
    buf.remove_prefix(last);
    if (!buf.empty()) {
        std::string val = static_cast<std::string>(buf);
        ToLower(val);
        ++document.words_appearance[val];
        ++word_count;
    }
}

void SearchEngine::CountTfIdf(std::vector<std::pair<double, size_t>>& tf_idf, const Document& words) const {
    std::vector<double> idf(words.words_appearance.size());

    size_t counter = 0;

    for (const auto& [word, word_counter] : words.words_appearance) {
        size_t rel_docs = 0;
        for (const auto& doc : documents_) {
            if (SubstrCount(doc, word) > 0) {
                ++rel_docs;
            }
        }
        idf[counter] = static_cast<double>(rel_docs) / documents_.size();
        ++counter;
    }

    for (size_t i = 0; i < documents_.size(); ++i) {
        counter = 0;
        for (const auto& [word, word_counter] : words.words_appearance) {
            tf_idf[i].first += static_cast<double>(SubstrCount(documents_[i], word)) /
                               documents_[i].words_appearance.size() * idf[counter];
            ++counter;
        }
    }
}

size_t SubstrCount(const Document& document, const std::string& sub) {
    if (document.words_appearance.find(sub) == document.words_appearance.end()) {
        return 0;
    } else {
        return document.words_appearance.at(sub);
    }
}

void ToLower(std::string& str) {
    for (auto& sym : str) {
        if (isupper(sym)) {
            sym = tolower(sym);
        }
    }
}