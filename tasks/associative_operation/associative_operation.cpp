#include "associative_operation.h"

bool IsAssociative(const std::vector<std::vector<size_t>>& table) {
    size_t a_size = table.size();
    for (size_t x = 0; x < a_size; ++x) {
        for (size_t y = 0; y < a_size; ++y) {
            for (size_t z = 0; z < a_size; ++z) {
                auto val1 = table[x][y];
                auto val2 = table[y][z];
                if (table[val1][z] != table[x][val2]) {
                    return false;
                }
            }
        }
    }
    return true;
}
