#include "tests_checking.h"
#include <deque>

std::vector<std::string> StudentsOrder(const std::vector<StudentAction>& student_actions,
                                       const std::vector<size_t>& queries) {
    std::deque<const std::string*> jobs;
    for (const auto& act : student_actions) {
        switch (act.side) {
            case Side::Top:
                jobs.push_front(&act.name);
                break;
            case Side::Bottom:
                jobs.push_back(&act.name);
                break;
        }
    }

    std::vector<std::string> ans;
    for (size_t i = 0; i < queries.size(); ++i) {
        ans.push_back(*jobs[queries[i] - 1]);
    }

    return ans;
}
