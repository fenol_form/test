#pragma once

#include <vector>
#include <sstream>
#include <map>
#include <math.h>

class Poly {
public:
    Poly();

    Poly(const std::vector<int64_t>& polynom);

    Poly(const std::vector<std::pair<int64_t, int64_t>>& polynom);

    Poly(const Poly& poly);

    friend bool operator==(const Poly& poly1, const Poly& poly2);

    friend bool operator!=(const Poly& poly1, const Poly& poly2);

    friend Poly operator+(const Poly& poly1, const Poly& poly2);

    friend Poly operator-(const Poly& poly1, const Poly& poly2);

    Poly operator-() const;

    Poly& operator+=(const Poly& poly2);

    Poly& operator-=(const Poly& poly2);

    friend Poly operator*(const Poly& poly1, const Poly& poly2);

    friend std::ostream& operator<<(std::ostream& stream, const Poly& poly);

    int64_t operator()(int64_t x_value) const;

private:
    std::map<int64_t, int64_t> polynom_;
};
