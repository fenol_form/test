#include "poly.h"

Poly::Poly() {
}

Poly::Poly(const std::vector<int64_t>& polynom) {
    for (size_t i = 0; i < polynom.size(); ++i) {
        if (polynom[i] != 0) {
            polynom_[i] = polynom[i];
        }
    }
}

Poly::Poly(const std::vector<std::pair<int64_t, int64_t>>& polynom) {
    for (size_t i = 0; i < polynom.size(); ++i) {
        if (polynom[i].second != 0) {
            polynom_[polynom[i].first] = polynom[i].second;
        }
    }
}

Poly::Poly(const Poly& poly) {
    polynom_ = poly.polynom_;
}

bool operator==(const Poly& poly1, const Poly& poly2) {
    return poly1.polynom_ == poly2.polynom_;
}

bool operator!=(const Poly& poly1, const Poly& poly2) {
    return !(poly1 == poly2);
}

Poly operator+(const Poly& poly1, const Poly& poly2) {
    Poly result;
    for (auto [d, c] : poly2.polynom_) {
        auto iter = poly1.polynom_.find(d);
        if (iter != poly1.polynom_.end() && iter->second != -c) {
            result.polynom_[d] += c + iter->second;
        }
    }
    return result;
}

Poly operator-(const Poly& poly1, const Poly& poly2) {
    return poly1 + (-poly2);
}

Poly Poly::operator-() const {
    Poly result = *this;
    for (auto& mon : result.polynom_) {
        mon.second *= -1;
    }
    return result;
}

Poly& Poly::operator+=(const Poly& poly2) {
    *this = *this + poly2;
    return *this;
}

Poly& Poly::operator-=(const Poly& poly2) {
    *this = *this - poly2;
    return *this;
}

Poly operator*(const Poly& poly1, const Poly& poly2) {
    Poly result;
    for (const auto& [d1, c1] : poly1.polynom_) {
        for (const auto& [d2, c2] : poly2.polynom_) {
            result.polynom_[d1 + d2] += c1 * c2;
        }
    }
    return result;
}

std::ostream& operator<<(std::ostream& stream, const Poly& poly) {
    stream << "y = ";
    if (poly.polynom_.empty()) {
        stream << "0";
        return stream;
    }
    std::vector<const std::pair<const int64_t, int64_t>*> sort_poly;
    sort_poly.reserve(poly.polynom_.size());
    for (const auto& mon : poly.polynom_) {
        sort_poly.push_back(&mon);
    }

    int i = 0;
    for (auto it = --poly.polynom_.end(); it != poly.polynom_.begin(); --it) {
        if (i > 0) {
            if (it->second > 0) {
                stream << " + ";
            } else {
                stream << " - ";
            }
            stream << abs(it->second);
            if (it->first > 0) {
                stream << "x^" << it->first;
            }
        } else {
            stream << it->second;
            if (it->first > 0) {
                stream << "x^" << it->first;
            }
        }
        ++i;
    }
    if (poly.polynom_.begin()->second > 0) {
        stream << " + ";
    } else {
        stream << " - ";
    }
    stream << abs(poly.polynom_.begin()->second);
    if (poly.polynom_.begin()->first > 0) {
        stream << "x^" << poly.polynom_.begin()->first;
    }
    return stream;
}

int64_t Poly::operator()(int64_t x_value) const {
    int64_t result = 0;
    for (const auto& [d, c] : polynom_) {
        result += powl(x_value, d) * c;
    }
    return result;
}
