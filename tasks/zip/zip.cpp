#include "zip.h"

#include <iostream>

Zipped Zip(Iterator a_begin, Iterator a_end, Iterator b_begin, Iterator b_end) {
    return Zipped(a_begin, a_end, b_begin, b_end);
}

Zipped::Zipped(Iterator ptr_first_begin, Iterator ptr_first_end, Iterator ptr_second_begin, Iterator ptr_second_end) :
        ptr_first_begin_(ptr_first_begin), ptr_first_end_(ptr_first_end), ptr_second_begin_(ptr_second_begin),
        ptr_second_end_(ptr_second_end) {
}

Zipped::Zip_Iterator Zipped::begin() {
    return Zip_Iterator(ptr_first_begin_, ptr_second_begin_);
}

Zipped::Zip_Iterator Zipped::end() {
    return Zip_Iterator(ptr_first_end_, ptr_second_end_);
}

Zipped::Zip_Iterator::Zip_Iterator(Iterator ptr_first, Iterator ptr_second) : ptr_first_(ptr_first),
        ptr_second_(ptr_second) {
}

ZippedPair Zipped::Zip_Iterator::operator*() const {
    return ZippedPair(*ptr_first_, *ptr_second_);
}

Zipped::Zip_Iterator& Zipped::Zip_Iterator::operator++() {
    ++ptr_first_;
    ++ptr_second_;
    return *this;
}

bool Zipped::Zip_Iterator::operator==(const Zip_Iterator& it) const {
    return ptr_first_ == it.ptr_first_ || ptr_second_ == it.ptr_second_;
}

bool Zipped::Zip_Iterator::operator!=(const Zip_Iterator& it) const {
    return !((*this) == it);
}

