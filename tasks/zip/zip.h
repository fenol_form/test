#pragma once

#include <forward_list>
#include <string>

using Value = std::string;
using Iterator = std::forward_list<std::string>::const_iterator;
using ZippedPair = std::pair<const Value&, const Value&>;

class Zipped {
public:
    Zipped(Iterator ptr_first_begin, Iterator ptr_first_end, Iterator ptr_second_begin,
           Iterator ptr_second_end);

    class Zip_Iterator {
    public:
        Zip_Iterator(Iterator ptr_first, Iterator ptr_second);

        ZippedPair operator*() const;

        Zip_Iterator& operator++();

        bool operator==(const Zip_Iterator& it) const;

        bool operator!=(const Zip_Iterator& it) const;

    private:
        Iterator ptr_first_;
        Iterator ptr_second_;
    };

    Zip_Iterator begin();   //NOLINT

    Zip_Iterator end();     //NOLINT

private:
    Iterator ptr_first_begin_, ptr_first_end_, ptr_second_begin_, ptr_second_end_;
};

Zipped Zip(Iterator a_begin, Iterator a_end, Iterator b_begin, Iterator b_end);
