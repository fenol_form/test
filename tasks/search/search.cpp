#include "search.h"

#include <algorithm>
#include <math.h>
#include <set>

bool ConditionSplitDocuments(char c) {
    return c == '\n';
}

bool ConditionSplitWords(char c) {
    return !isalpha(c);
}

void Split(std::vector<std::string_view>& documents, const std::string_view& text, bool (*cond)(char)) {
    size_t last = 0;
    size_t cur = 0;
    for (size_t i = 0; i < text.size(); ++i) {
        if (cond(text[i])) {
            std::string_view buf = text;
            buf.remove_prefix(last);
            buf.remove_suffix(text.size() - cur);
            documents.push_back(buf);
            last = cur + 1;
        }
        ++cur;
    }
    std::string_view buf = text;
    buf.remove_prefix(last);
    documents.push_back(buf);
}

void DeleteSameWords(std::vector<std::string_view>& words) {
    std::set<std::string_view> set_words;
    for (const auto& word : words) {
        set_words.insert(word);
    }
    words.clear();
    words.reserve(set_words.size());
    for (const auto& word : set_words) {
        words.push_back(word);
    }
}

size_t CountWords(const std::string_view& text) {
    size_t ans = 0;
    bool last = false;
    for (const auto& sym : text) {
        if (!std::isalpha(sym) && last == true) {
            ++ans;
            last = false;
        }
        if (std::isalpha(sym)) {
            last = true;
        }
    }
    if (last == true) {
        ++ans;
    }
    return ans;
}

size_t SubstrCount(const std::string_view& text, const std::string_view& sub) {
    size_t ans = 0;
    std::vector<std::string_view> words;
    Split(words, text, ConditionSplitWords);
    auto pos = std::find(words.begin(), words.end(), sub);
    while (pos < words.end()) {
        ++ans;
        pos = std::find(pos + 1, words.end(), sub);
    }
    return ans;
}

void CountTfIdf(std::vector<std::pair<double, size_t>>& tf_idf, const std::vector<std::string_view>& documents,
                const std::vector<std::string_view>& words) {

    std::vector<double> idf(words.size());

    for (size_t i = 0; i < words.size(); ++i) {
        size_t rel_docs = 0;
        for (const auto& doc : documents) {
            if (SubstrCount(doc, words[i]) > 0) {
                ++rel_docs;
            }
        }
        idf[i] = static_cast<double>(rel_docs) / documents.size();
    }

    for (size_t i = 0; i < documents.size(); ++i) {
        for (size_t j = 0; j < words.size(); ++j) {
            tf_idf[i].first +=
                (static_cast<double>(SubstrCount(documents[i], words[j])) / CountWords(documents[i])) * idf[j];
        }
    }
}

void ToLower(std::string_view& text, std::string& text_copy) {
    for (auto& sym : text_copy) {
        if (isupper(sym)) {
            sym = tolower(sym);
        }
    }
    text = text_copy;
}

std::vector<std::string_view> Search(std::string_view text, std::string_view query, size_t results_count) {
    std::vector<std::string_view> words;
    std::vector<std::string_view> result;
    std::vector<std::string_view> documents;
    std::vector<std::string_view> documents_orig;
    Split(documents_orig, text, ConditionSplitDocuments);

    std::string text_copy(text);
    std::string query_copy(query);
    ToLower(text, text_copy);
    ToLower(query, query_copy);
    Split(documents, text, ConditionSplitDocuments);
    Split(words, query, ConditionSplitWords);
    DeleteSameWords(words);

    std::vector<std::pair<double, size_t>> tf_idf(documents.size(), {0, 0});
    for (size_t i = 0; i < tf_idf.size(); ++i) {
        tf_idf[i].second = i;
    }

    CountTfIdf(tf_idf, documents, words);

    std::sort(
        tf_idf.begin(), tf_idf.end(),
        [](const std::pair<double, int>& a, const std::pair<double, int>& b) -> bool { return a.first > b.first; });

    for (size_t i = 0; i < std::min(results_count, tf_idf.size()); ++i) {
        if (tf_idf[i].first <= 0) {
            break;
        }
        result.push_back(documents_orig[tf_idf[i].second]);
    }

    return result;
}
