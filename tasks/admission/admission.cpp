#include "admission.h"
#include <tuple>
#include <map>

bool Compare(const Applicant* app1, const Applicant* app2) {
    return std::tuple(-app1->points, app1->student.birth_date.year, app1->student.birth_date.month,
                      app1->student.birth_date.day, app1->student.name) <
           std::tuple(-app2->points, app2->student.birth_date.year, app2->student.birth_date.month,
                      app2->student.birth_date.day, app2->student.name);
}

AdmissionTable FillUniversities(const std::vector<University>& universities, const std::vector<Applicant>& applicants) {
    std::vector<const Applicant*> sort_applicants(applicants.size());
    std::map<std::string, size_t> universities_map;
    AdmissionTable result;

    for (size_t i = 0; i < applicants.size(); ++i) {
        sort_applicants[i] = &applicants[i];
    }
    sort(sort_applicants.begin(), sort_applicants.end(), Compare);

    for (const auto& university : universities) {
        universities_map[university.name] = university.max_students;
    }
    for (const auto app : sort_applicants) {
        for (const auto& name : app->wish_list) {
            if (universities_map[name] > 0) {
                --universities_map[name];
                result[name].push_back(&app->student);
                break;
            }
        }
    }
    return result;
}
