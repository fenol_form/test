#include "multiplication.h"

#include <stdexcept>

int64_t Multiply(int a, int b) {
    return (long long)a * b;
}
