#include "password.h"
#include <cctype>

bool ValidatePassword(const std::string& password) {
    const unsigned int min_length = 8;
    const unsigned int max_length = 14;
    const char min_letter = 33;
    const char max_letter = 126;
    if (password.size() < min_length || password.size() > max_length) {
        return false;
    }
    bool upper = false;
    bool lower = false;
    bool digit = false;
    bool other = false;
    for (size_t i = 0; i < password.size(); ++i) {
        char let = password[i];
        size_t code = let;
        if (code < min_letter || code > max_letter) {
            return false;
        }
        if (std::isupper(let)) {
            upper = true;
            continue;
        }
        if (std::islower(let)) {
            lower = true;
            continue;
        }
        if (std::isdigit(let)) {
            digit = true;
            continue;
        }
        other = true;
    }
    unsigned int count_classes_syms = 0;
    if (upper == true) {
        count_classes_syms++;
    }
    if (lower == true) {
        count_classes_syms++;
    }
    if (digit == true) {
        count_classes_syms++;
    }
    if (other == true) {
        count_classes_syms++;
    }
    return count_classes_syms >= 3;
}
