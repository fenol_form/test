#include "word2vec.h"

#include <vector>
#include <algorithm>

int64_t GetMetric(const std::vector<int>& first, const std::vector<int>& second) {
    int64_t ans = 0;
    for (size_t i = 0; i < first.size(); ++i) {
        ans += first[i] * second[i];
    }
    return ans;
}

std::vector<std::string> FindClosestWords(const std::vector<std::string>& words,
                                          const std::vector<std::vector<int>>& vectors) {
    if (words.size() == 1) {
        return {};
    }
    int64_t max_metric = INT64_MIN;
    std::vector<std::string> ans;
    for (size_t i = 1; i < words.size(); ++i) {
        int64_t cur_metric = GetMetric(vectors[0], vectors[i]);
        if (cur_metric > max_metric) {
            ans.clear();
            max_metric = cur_metric;
            ans.push_back(words[i]);
        } else {
            if (cur_metric == max_metric) {
                ans.push_back(words[i]);
            }
        }
    }
    return ans;
}
