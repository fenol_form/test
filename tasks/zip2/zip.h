#pragma once

#include <iterator>

template <typename Iterator1, typename Iterator2>
class Zipped {
public:
    Zipped(Iterator1 ptr_first_begin, Iterator1 ptr_first_end, Iterator2 ptr_second_begin, Iterator2 ptr_second_end) :
          ptr_first_begin_(ptr_first_begin), ptr_first_end_(ptr_first_end), ptr_second_begin_(ptr_second_begin),
          ptr_second_end_(ptr_second_end) {
    }

    class Zip_Iterator {
    public:
        Zip_Iterator(Iterator1 ptr_first, Iterator2 ptr_second) : ptr_first_(ptr_first),
              ptr_second_(ptr_second) {
        }

        auto operator*() const {
            return std::pair(*ptr_first_, *ptr_second_);
        }

        Zip_Iterator& operator++() {
            ++ptr_first_;
            ++ptr_second_;
            return *this;
        }

        bool operator==(const Zip_Iterator& it) const {
            return ptr_first_ == it.ptr_first_ || ptr_second_ == it.ptr_second_;
        }

        bool operator!=(const Zip_Iterator& it) const {
            return !(*this == it);
        }

    private:
        Iterator1 ptr_first_;
        Iterator2 ptr_second_;
    };

    Zip_Iterator begin() {  //NOLINT
        return Zip_Iterator(ptr_first_begin_, ptr_second_begin_);
    }

    Zip_Iterator end() {    //NOLINT
        return Zip_Iterator(ptr_first_end_, ptr_second_end_);
    }

private:
    Iterator1 ptr_first_begin_;
    Iterator1 ptr_first_end_;
    Iterator2 ptr_second_begin_;
    Iterator2 ptr_second_end_;
};

template <typename Sequence1, typename Sequence2>
auto Zip(const Sequence1& sequence1, const Sequence2& sequence2) {
    return Zipped(std::begin(sequence1), std::end(sequence1), std::begin(sequence2), std::end(sequence2));
}