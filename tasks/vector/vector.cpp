#include "vector.h"

#include <iostream>
#include <cstring>

Vector::Vector() : capacity_(0), size_(0), array_(nullptr) {
}

Vector::Vector(size_t size) : capacity_(size), size_(size), array_(new ValueType[size]{0}) {
}

Vector::Vector(std::initializer_list<ValueType> list)
    : capacity_(list.size()), size_(list.size()), array_(new ValueType[list.size()]) {
    for (auto it = list.begin(); it != list.end(); ++it) {
        array_[it - list.begin()] = *it;
    }
}

Vector::Vector(const Vector& other) {
    (*this) = other;
}

Vector& Vector::operator=(const Vector& other) {
    if (*this != other) {
        size_ = other.size_;
        capacity_ = other.capacity_;
        Reserve(other.size_);
        for (size_t i = 0; i < other.size_; ++i) {
            array_[i] = other.array_[i];
        }
        return (*this);
    }
    return *this;
}

Vector::~Vector() {
    delete[] array_;
}

Vector::SizeType Vector::Size() const {
    return size_;
}

Vector::SizeType Vector::Capacity() const {
    return capacity_;
}

Vector::ValueType& Vector::operator[](size_t position) {
    return array_[position];
}

const Vector::ValueType& Vector::operator[](size_t position) const {
    return array_[position];
}

const Vector::ValueType* Vector::Data() const {
    return array_;
}

bool Vector::operator==(const Vector& other) const {
    if (size_ != other.size_) {
        return false;
    }
    for (size_t i = 0; i < size_; ++i) {
        if ((*this)[i] != other[i]) {
            return false;
        }
    }
    return true;
}

bool Vector::operator!=(const Vector& other) const {
    return !(*this == other);
}

std::strong_ordering Vector::operator<=>(const Vector& other) const {
    if (*(this) == other) {
        return std::strong_ordering::equal;
    }
    if (std::lexicographical_compare(array_, array_ + size_, other.array_, other.array_ + other.size_)) {
        return std::strong_ordering::less;
    } else {
        return std::strong_ordering::greater;
    }
}

void Vector::Reserve(SizeType new_capacity) {
    if (capacity_ < new_capacity) {
        ValueType* new_array = new ValueType[new_capacity];
        capacity_ = new_capacity;
        for (size_t i = 0; i < size_; ++i) {
            new_array[i] = array_[i];
        }
        delete[] array_;
        array_ = new_array;
    }
}

void Vector::Clear() {
    size_ = 0;
}

void Vector::PushBack(const ValueType& new_element) {
    if (capacity_ < size_ + 1) {
        if (capacity_ == 0) {
            capacity_ = 1;
        }
        Reserve(capacity_ * 2);
    }
    ++size_;
    array_[size_ - 1] = new_element;
}

void Vector::PopBack() {
    if (size_ > 0) {
        --size_;
    }
}

void Vector::Swap(Vector& other) {
    std::swap(size_, other.size_);
    std::swap(capacity_, other.capacity_);
    std::swap(array_, other.array_);
}

Vector::Iterator Vector::Begin() {
    return Iterator(array_);
}

Vector::Iterator Vector::End() {
    return Iterator(array_ + size_);
}

Vector::Iterator Vector::begin() {  // NOLINT
    return Begin();
}

Vector::Iterator Vector::end() {  // NOLINT
    return End();
}

Vector::Iterator::Iterator() : pointer_(nullptr) {
}

Vector::Iterator::Iterator(ValueType* pointer) : pointer_(pointer) {
}

Vector::ValueType& Vector::Iterator::operator*() const {
    return *pointer_;
}

Vector::ValueType* Vector::Iterator::operator->() const {
    return pointer_;
}

Vector::Iterator& Vector::Iterator::operator=(Iterator other) {
    pointer_ = other.pointer_;
    return *this;
}

Vector::Iterator& Vector::Iterator::operator++() {
    ++pointer_;
    return *this;
}

Vector::Iterator Vector::Iterator::operator++(int) {
    Vector::Iterator temp = (*this);
    ++(*this);
    return temp;
}

Vector::Iterator& Vector::Iterator::operator--() {
    --pointer_;
    return *this;
}

Vector::Iterator Vector::Iterator::operator--(int) {
    Vector::Iterator temp = (*this);
    --(*this);
    return temp;
}

Vector::Iterator Vector::Iterator::operator+(DifferenceType shift) {
    return Vector::Iterator(pointer_ + shift);
}

Vector::SizeType Vector::Iterator::operator-(Iterator other) {
    return pointer_ - other.pointer_;
}

Vector::Iterator& Vector::Iterator::operator+=(DifferenceType shift) {
    (*this) = (*this) + shift;
    return (*this);
}

Vector::Iterator& Vector::Iterator::operator-=(DifferenceType shift) {
    pointer_ -= shift;
    return (*this);
}

bool Vector::Iterator::operator==(const Iterator& other) const {
    return pointer_ == other.pointer_;
}

bool Vector::Iterator::operator!=(const Iterator& other) const {
    return !((*this) == other);
}

std::strong_ordering Vector::Iterator::operator<=>(const Iterator& other) const {
    if (pointer_ - other.pointer_ < 0) {
        return std::strong_ordering::less;
    }
    if (pointer_ - other.pointer_ > 0) {
        return std::strong_ordering::greater;
    }
    return std::strong_ordering::equal;
}
