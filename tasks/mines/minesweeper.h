#pragma once

#include <string>
#include <vector>
#include <ctime>
#include <queue>
#include <algorithm>
#include <random>

size_t FixIndex(size_t i);

const char ZERO_CODE = 48;

class Minesweeper {
public:
    struct Cell {
        size_t x = 0;
        size_t y = 0;
    };

    struct CellStatus {
        bool mine = false;
        bool open = false;
        bool flag = false;
    };

    enum class GameStatus {
        NOT_STARTED,
        IN_PROGRESS,
        VICTORY,
        DEFEAT,
    };

    using RenderedField = std::vector<std::string>;

    Minesweeper(size_t width, size_t height, size_t mines_count);
    Minesweeper(size_t width, size_t height, const std::vector<Cell>& cells_with_mines);

    void NewGame(size_t width, size_t height, size_t mines_count);
    void NewGame(size_t width, size_t height, const std::vector<Cell>& cells_with_mines);

    void OpenCell(const Cell& cell);
    void MarkCell(const Cell& cell);

    GameStatus GetGameStatus() const;
    time_t GetGameTime() const;

    RenderedField RenderField() const;

private:
    std::vector<std::vector<CellStatus>> field_;
    GameStatus current_game_status_;
    size_t open_cells_count_;
    size_t mines_count_;
    size_t width_, height_;
    time_t start_time_, stop_time_;

    void GetCellAround(const Cell& cell, std::vector<Cell>& result) const;

    size_t CountMinesAround(const Cell& cell) const;

    void BfSearch(const Cell& cell);
};
