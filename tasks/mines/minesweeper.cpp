#include "minesweeper.h"

Minesweeper::Minesweeper(size_t width, size_t height, size_t mines_count) {
    NewGame(width, height, mines_count);
}

Minesweeper::Minesweeper(size_t width, size_t height, const std::vector<Cell>& cells_with_mines) {
    NewGame(width, height, cells_with_mines);
}

void Minesweeper::NewGame(size_t width, size_t height, size_t mines_count) {
    std::vector<Cell> cells(width * height);
    for (size_t i = 0; i < height; ++i) {
        for (size_t j = 0; j < width; ++j) {
            cells[i * height + j] = {j, i};
        }
    }
    std::shuffle(cells.begin(), cells.end(), std::default_random_engine(std::time(nullptr)));
    cells.resize(mines_count);
    NewGame(width, height, cells);
}

void Minesweeper::NewGame(size_t width, size_t height, const std::vector<Cell>& cells_with_mines) {
    current_game_status_ = GameStatus::NOT_STARTED;
    field_ = std::vector<std::vector<CellStatus>>(height, std::vector(std::vector<CellStatus>(width)));
    open_cells_count_ = 0;
    mines_count_ = cells_with_mines.size();
    width_ = width;
    height_ = height;
    start_time_ = std::time(nullptr);
    stop_time_ = start_time_;
    for (const auto& cell : cells_with_mines) {
        field_[cell.y][cell.x].mine = true;
    }
}

void Minesweeper::OpenCell(const Cell& cell) {
    if (current_game_status_ == GameStatus::NOT_STARTED) {
        current_game_status_ = GameStatus::IN_PROGRESS;
    }
    if (current_game_status_ != GameStatus::IN_PROGRESS || field_[cell.y][cell.x].flag) {
        return;
    }
    if (field_[cell.y][cell.x].mine) {
        stop_time_ = std::time(nullptr);
        current_game_status_ = GameStatus::DEFEAT;
        for (auto& str : field_) {
            for (auto& el : str) {
                el.open = true;
            }
        }
        return;
    }

    BfSearch(cell);
    if (open_cells_count_ + mines_count_ == width_ * height_) {
        current_game_status_ = GameStatus::VICTORY;
        stop_time_ = std::time(nullptr);
    }
}

void Minesweeper::MarkCell(const Cell& cell) {
    if (current_game_status_ == GameStatus::NOT_STARTED) {
        current_game_status_ = GameStatus::IN_PROGRESS;
    }
    if (current_game_status_ == GameStatus::IN_PROGRESS && !field_[cell.y][cell.x].open) {
        field_[cell.y][cell.x].flag = !field_[cell.y][cell.x].flag;
    }
}

Minesweeper::GameStatus Minesweeper::GetGameStatus() const {
    return current_game_status_;
}

time_t Minesweeper::GetGameTime() const {
    if (current_game_status_ == GameStatus::IN_PROGRESS) {
        return std::time(nullptr) - start_time_;
    } else {
        return stop_time_ - start_time_;
    }
}

Minesweeper::RenderedField Minesweeper::RenderField() const {
    RenderedField result(height_);
    for (size_t i = 0; i < height_; ++i) {
        for (size_t j = 0; j < width_; ++j) {
            if (field_[i][j].open) {
                if (field_[i][j].mine) {
                    result[i].push_back('*');
                } else {
                    size_t mines_around = CountMinesAround({j, i});
                    if (mines_around == 0) {
                        result[i].push_back('.');
                    } else {
                        result[i].push_back(mines_around + ZERO_CODE);
                    }
                }
            } else {
                if (field_[i][j].flag) {
                    result[i].push_back('?');
                } else {
                    result[i].push_back('-');
                }
            }
        }
    }
    return result;
}

void Minesweeper::GetCellAround(const Cell& cell, std::vector<Cell>& result) const {
    for (size_t i = FixIndex(cell.y - 1); i < cell.y + 2; ++i) {
        for (size_t j = FixIndex(cell.x - 1); j < cell.x + 2; ++j) {
            if (i < height_ && j < width_ && (i != cell.y || j != cell.x)) {
                result.push_back({static_cast<size_t>(j), static_cast<size_t>(i)});
            }
        }
    }
}

size_t Minesweeper::CountMinesAround(const Cell& cell) const {
    size_t ans = 0;
    std::vector<Cell> cells_around;
    GetCellAround(cell, cells_around);
    for (const auto& el : cells_around) {
        ans += field_[el.y][el.x].mine ? 1 : 0;
    }
    return ans;
}

void Minesweeper::BfSearch(const Cell& cell) {
    std::queue<Cell> q;
    q.push(cell);
    while (!q.empty()) {
        auto current_cell = q.front();
        q.pop();
        if (field_[current_cell.y][current_cell.x].mine || field_[current_cell.y][current_cell.x].open ||
            field_[current_cell.y][current_cell.x].flag) {
            continue;
        }
        field_[current_cell.y][current_cell.x].open = true;
        ++open_cells_count_;
        if (CountMinesAround(current_cell) == 0) {
            std::vector<Cell> cells_around;
            GetCellAround(current_cell, cells_around);
            for (const auto& el : cells_around) {
                q.push(el);
            }
        }
    }
}

size_t FixIndex(size_t i) {
    if (i == SIZE_MAX) {
        i = 0;
    }
    return i;
}