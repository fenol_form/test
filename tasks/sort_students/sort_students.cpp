#include "sort_students.h"
#include "tuple"

bool cmp_date(const Student& first_student, const Student& second_student) {
    return std::tie(first_student.birth_date.year, first_student.birth_date.month,
                                        first_student.birth_date.day, first_student.last_name,
                                        first_student.name) <
           std::tie(second_student.birth_date.year, second_student.birth_date.month,
                                         second_student.birth_date.day, second_student.last_name,
                                         second_student.name);
}

bool cmp_name(const Student& first_student, const Student& second_student) {
    return std::tie(first_student.last_name, first_student.name,
                                        first_student.birth_date.year, first_student.birth_date.month,
                                        first_student.birth_date.day) <
           std::tie(second_student.last_name, second_student.name,
                                         second_student.birth_date.year, second_student.birth_date.month,
                                         second_student.birth_date.day);
}

void SortStudents(std::vector<Student>& students, SortKind sortKind) {
    if (sortKind == SortKind::Date) {
        std::sort(students.begin(), students.end(), cmp_date);
    } else {
        std::sort(students.begin(), students.end(), cmp_name);
    }
}
